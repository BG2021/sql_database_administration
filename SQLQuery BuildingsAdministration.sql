CREATE TABLE Employees
(
EmployeeID INT CONSTRAINT cst_emplID_pKey PRIMARY KEY IDENTITY (1,1),
LastName VARCHAR (20) NOT NULL,
FirstName VARCHAR (20) NOT NULL,
HireDate DATETIME,
Phone CHAR (9) NOT NULL,
ModifiedDate DATETIME NOT NULL DEFAULT GETDATE ())

INSERT INTO Employees (LastName, FirstName, HireDate, Phone) 
	VALUES ('Kowalski', 'Jan', '2018-02-01', 111222333)
INSERT INTO Employees (LastName, FirstName, HireDate, Phone) 
	VALUES ('Nowak', 'Andrzej', '2019-01-01', 222222444)
INSERT INTO Employees (LastName, FirstName, HireDate, Phone) 
	VALUES ('Wi�niewski', 'Piotr', '2019-01-01', 555222444)

CREATE TABLE Customers
(
CustomerID INT CONSTRAINT cst_clientID_pKey PRIMARY KEY IDENTITY (1,1),
CustomerName VARCHAR (100) CONSTRAINT cst_clientName_unique UNIQUE NOT NULL,
City VARCHAR (20) NOT NULL,
PostalCode CHAR (5) NOT NULL,
Addres VARCHAR (100) NOT NULL,
EmployeeID INT CONSTRAINT cst_emplID_fKey FOREIGN KEY REFERENCES Employees(EmployeeID),
ModifiedDate DATETIME NOT NULL DEFAULT GETDATE ())

INSERT INTO Customers (CustomerName, City, PostalCode, Addres, EmployeeID) 
	VALUES ('Wsp�lnota Mieszkaniowa Carrara', 'Gliwice', 44100, 'Nowy �wiat 41', 1)
INSERT INTO Customers (CustomerName, City, PostalCode, Addres, EmployeeID)
	VALUES ('Wsp�lnota Mieszkaniowa Ferrara', 'Rybnik', 44200, 'Miko�owska 22', 3)
INSERT INTO Customers (CustomerName, City, PostalCode, Addres, EmployeeID)
	VALUES ('Wsp�lnota Gara�owa Le�na', 'Gliwice', 44105, 'Le�na 89', 2)
INSERT INTO Customers (CustomerName, City, PostalCode, Addres, EmployeeID)
	VALUES ('Wsp�lnota Gara�owa Zielona', 'Rybnik', 44200, 'Zielona 101', 1)

CREATE TABLE Appartments
(
AppartmentID INT CONSTRAINT cst_app_pKey PRIMARY KEY IDENTITY (1,1),
CustomerID INT CONSTRAINT cst_custom_fKey FOREIGN KEY REFERENCES Customers(CustomerID),
StairCase CHAR (1),
AppartmentNumber INT NOT NULL,
Area FLOAT (2) NOT NULL,
ModifiedDate DATETIME NOT NULL DEFAULT GETDATE ())

INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (1, 'A', 1, 45.25)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (1, 'A', 2, 46.25)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (1, 'A', 3, 46.28)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (1, 'B', 1, 45.25)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (1, 'B', 2, 46.25)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (1, 'B', 3, 46.28)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (2, 'A', 1, 55.25)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (2, 'A', 2, 56.25)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (2, 'B', 3, 66.25)
INSERT INTO Appartments (CustomerID, StairCase, AppartmentNumber, Area) 
	VALUES (2, 'B', 4, 67.25)

CREATE TABLE Garages
(
GarageID INT CONSTRAINT cst_gar_pKey PRIMARY KEY IDENTITY (1,1),
CustomerID INT CONSTRAINT cst_gcustom_fKey FOREIGN KEY REFERENCES Customers(CustomerID),
GarageNumber INT NOT NULL,
Area FLOAT (2) NOT NULL,
ModifiedDate DATETIME NOT NULL DEFAULT GETDATE ())

INSERT INTO Garages (CustomerID, GarageNumber, Area) 
	VALUES (3, 1, 22.25)
INSERT INTO Garages (CustomerID, GarageNumber, Area) 
	VALUES (3, 2, 22.20)
INSERT INTO Garages (CustomerID, GarageNumber, Area) 
	VALUES (3, 3, 22.40)
INSERT INTO Garages (CustomerID, GarageNumber, Area) 
	VALUES (3, 4, 23.40)
INSERT INTO Garages (CustomerID, GarageNumber, Area) 
	VALUES (4, 1, 20.25)
INSERT INTO Garages (CustomerID, GarageNumber, Area) 
	VALUES (4, 2, 19.20)
INSERT INTO Garages (CustomerID, GarageNumber, Area) 
	VALUES (4, 3, 19.40)


CREATE TABLE Proprietors
(
ProprietorID INT CONSTRAINT cst_propr_pKey PRIMARY KEY IDENTITY (1,1),
LastName VARCHAR (20) NOT NULL,
FirstName VARCHAR (20) NOT NULL,
City VARCHAR (20) NOT NULL,
PostalCode CHAR (5) NOT NULL,
Addres VARCHAR (100) NOT NULL,
Phone CHAR (9) CONSTRAINT cst_tel_unique UNIQUE,
ModifiedDate DATETIME NOT NULL DEFAULT GETDATE ())

INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('W�jcik', 'Julia', 'Gliwice', 44100, 'Podmiejska 27/1', 345567789)
INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('W�jcik', 'Szymon', 'Gliwice', 44100, 'Podmiejska 27/1', 333444555)
INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('Kowalczyk', 'Justyna', 'Zabrze', 44300, 'Francuska 33', 567789999)
INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('Kami�ski', 'Zbigniew', 'Gliwice', 44100, 'Miejska 12/8', 123123123)
INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('Kami�ska', 'Zofia', 'Gliwice', 44100, 'Miejska 12/8', 333456667)
INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('Lewandowski', 'Robert', 'Rybnik', 44200, 'Le�na 100', 555666777)
INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('Zieli�ski', 'Tomasz', 'Krak�w', 55200, 'Wawelska 5', 898787676)
INSERT INTO Proprietors (LastName, FirstName, City, PostalCode, Addres, Phone) 
	VALUES ('Szyma�ska', 'Halina', 'Zabrze', 44300, 'Gliwicka 5', 555444555)

CREATE TABLE AppartmentsProperty
(
PropertyID INT CONSTRAINT cst_prope_pKey PRIMARY KEY IDENTITY (1,1),
AppartmentID INT CONSTRAINT cst_app_fKey FOREIGN KEY REFERENCES Appartments(AppartmentID),
PropietorID INT CONSTRAINT cst_prop_fKey FOREIGN KEY REFERENCES Proprietors(ProprietorID))

INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (1,1)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (1,2)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (2,3)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (3,4)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (3,5)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (4,6)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (5,7)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (6,7)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (7,8)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (7,7)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (8,1)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (8,2)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (9,8)
INSERT INTO AppartmentsProperty (AppartmentID, PropietorID) 
	VALUES (10,8)

CREATE TABLE GaragesProperty
(
PropertyID INT CONSTRAINT cst_Gprope_pKey PRIMARY KEY IDENTITY (1,1),
GarageID INT CONSTRAINT cst_Gapp_fKey FOREIGN KEY REFERENCES Garages(GarageID),
PropietorID INT CONSTRAINT cst_Gprop_fKey FOREIGN KEY REFERENCES Proprietors(ProprietorID))

INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (1,1)
INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (1,2)
INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (2,2)
INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (3,3)
INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (4,5)
INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (5,5)
INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (6,3)
INSERT INTO GaragesProperty (GarageID, PropietorID) 
	VALUES (7,5)

/*Utw�rz list� wszystkich nieruchomo�ci, zawieraj�c� adres, numer klatki i powierzchni� (gara� do 20 lub od 20m2,
mieszkanie do 50m2 lub od 50m2), b�d�cych w�asno�ci� lub wsp�w�asno�ci� Szymona W�jcika. Zapytanie modyfikowalne dla r�nych .*/

DECLARE @LastName AS VARCHAR (20) = 'W�jcik';
DECLARE @FirstName AS VARCHAR (20) = 'Szymon';
SELECT
C.Addres + A.StairCase + ', ' + C.PostalCode + ' ' + C.City AS Adres,
A.AppartmentNumber AS [Numer lokalu],
CASE 
	WHEN A.Area < 50 THEN 'do 50m2'
	WHEN A.Area >= 50 THEN '50m2 i wi�ksze' END AS Powierzchnia,
'Mieszkanie' AS Typ
FROM Appartments AS A JOIN AppartmentsProperty AS AP ON A.AppartmentID=AP.AppartmentID 
JOIN Proprietors AS P ON AP.PropietorID=P.ProprietorID JOIN Customers AS C ON C.CustomerID=A.CustomerID
WHERE P.LastName LIKE @LastName AND P.FirstName LIKE @FirstName

UNION

SELECT
C.Addres + ', ' + C.PostalCode + ' ' + C.City AS Adres,
G.GarageNumber AS [Numer lokalu],
CASE 
	WHEN G.Area < 20 THEN 'do 20m2'
	WHEN G.Area >= 20 THEN '20m2 i wi�ksze' END AS Powierzchnia,
'Gara�' AS Typ
FROM Garages AS G JOIN GaragesProperty AS GP ON G.GarageID=GP.GarageID 
JOIN Proprietors AS P ON GP.PropietorID=P.ProprietorID JOIN Customers AS C ON C.CustomerID=G.CustomerID
WHERE P.LastName LIKE @LastName AND P.FirstName LIKE @FirstName

